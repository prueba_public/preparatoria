<!DOCTYPE html>
<html lang="es">
<?php
      include_once 'php/head.php';
      require 'admin/php/config.php';
	  
      ?>
<body style="background:#fff;">
<?php
      include_once 'php/header.php';
      
	  
      ?>

      <section class="container" id="articulos">
         <div class="row">
            <div class="col-md-12">
               <div id="prevArrow"></div>
               <ul class="bxslider">
                  <?php
                     $bannerConsulta = "SELECT imagen, id FROM pricipal where categoria='noticias' ORDER by id desc LIMIT 3";
                      if($resultado = mysqli_query($con, $bannerConsulta)){
                        while ($row = mysqli_fetch_assoc($resultado))  {
                     ?>
                  <li><a href="interior2.php?interior=<?php echo $row['id']?>"><img src="<?php echo $row['imagen']?>"/></a></li>
                  <?php
                     }
                     mysqli_free_result($resultado);
                     }else{
                     echo "No hay resultados";
                     }
                     ?>
               </ul>
               <div id="nextArrow"></div>
            </div>
         </div>
      </section>
	
	
<div class="container">
	<hr class="line">
	<div class="form-group">
	
	<?php
         $query="SELECT imagen, id,fecha,titulo,parrafo FROM pricipal where categoria='noticias' ORDER by id desc LIMIT 5";
          
         $resultado=$con->query($query);  
         $con ->query("SET NAMES 'utf8'");//sentencia de reconocimiento de acentos y ñ
         
         $i = 0; 
         ?>
	
		<section class="col-md-8">
					 <?php 
                     while($row = mysqli_fetch_assoc($resultado)){ 
                     ?>
		<article>


		
		<div class="row">
			<div class="col-md-5">
			
			  <a href="interior2.php?interior=<?php echo $row['id']?>">
                       <img class="img-rounded img-responsive" src="<?php echo $row['imagen']?>" alt="" id="img_principal">
                           </a>
			
				
			</div>
			
			<div class="form-group">
				<p class="titulo_seccion"><?php echo $row['titulo']?></p>
				 <div class="fecha"><?php echo $row['fecha']?></div>
                     <div class="intro"><?php echo $row['parrafo']?></div>
                     <div class="link">
                        <a href="interior2.php?interior=<?php echo $row['id']?>">Seguir Leyendo...</a>
                     </div>
				
			</div>
			
		</div>
		
		</article>
		 <?php } ?>
					  
		</section>
		<aside class="col-md-4">
		<div class="sidebar">
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title " >
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color:#598eca;">
						<b>ADMINISTRACIÓN ESCOLAR</b></a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
							<table class="table">
							<tr>
								<td>
									<a href="archivos/requsitos.pdf"> Transito de Alumnos</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> Programa yo no abandono la escuela </a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> Lineamientos y portabilidad</a>
								</td>
							</tr>
							
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"  style="color:#598eca;">
						 <b>NOTICIAS </b></a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table">
							<tr>
								<td>
									<a href="interior.php?interior=Semana de la  Ciencia"> Dia Internacional de la Mujer</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> Combatir el abuso Escolar</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href="interior.php?interior=2 de octubre no se olvida"> Evento del 2 de octubre</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href="interior.php?interior=muestra gastronomica"> Muestra Gastronómica</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> Dia de Muertos</a>
								</td>
							</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="color:#598eca;">
						 <b>PLAN DE ESTUDIOS</b> </a>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table">
							<tr>
								<td>
									<a href=""> I Semestre </a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> II Semestre </a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> II Semestre </a>
								</td>
							</tr>
							<tr>
								<td>
								<a href="">IV Semestre</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> V Semestre </a>
								</td>
							</tr>
							<tr>
								<td>
									<a href=""> VI Semestre </a>
								</td>
							</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color:#598eca;">
					 <b>CALIFICACIONES</b></a>
						</h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							<table class="table">
							<tr>
								<td>
									<a href=""> Pagina del Gobierno del Edo.Mex</a>
								</td>
							</tr>
							
							</table>
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="well">
					<h4><i class="fa fa-list"></i> Eventos y Acciones</h4>
					<ul>
						<li><a href="">Reinscripciones</a></li>
						<li><a href="">Guias de Estudio</a></li>
						<li><a href="">Videos Escolares</a></li>
						<li><a href="">Entrega de Utiles</a></li>
						<li><a href="">Planes y Programas de Estudio</a></li>
						<li><a href="">Examenes Extraordinarios</a></li>
					</ul>
				</div>
			</div>
			<br>
			<br>
			<div class="panel-group" id="accordion">
				<!--<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						<span class="glyphicon glyphicon-folder-close">
						</span> Visitas </a>
						</h4>
					</div>
					<div class="panel-body">
						<table class="table">
						<tr>
							<td>
								<span class="glyphicon glyphicon-pencil text-primary"></span><a href=""> Reunion con padres </a>
							</td>
						</tr>
						<tr>
							<td>
								<span class="glyphicon glyphicon-flash text-success"></span><a href=""> Junta de Escuela </a>
							</td>
						</tr>
						<tr>
							<td>
								<span class="glyphicon glyphicon-file text-info"></span><a href=""> Tallere </a>
							</td>
						</tr>
						<tr>
							<td>
								<span class="glyphicon glyphicon-comment text-success"></span><a href=""> Comments </a>
							</td>
						</tr>
						</table>
					</div>
				</div>-->
				<br>
				<br>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						<b> Talleres</b> </a>
						</h4>
					</div>
					<div class="panel-body">
						<table class="table">
						<tr>
							<td>
								<a href=""> Taller de Ingles </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href=""> Taller de Artes Plasticas </a>
							</td>
						</tr>
						<tr>
							<td>
								<a href=""> Taller de Mascaras</a>
							</td>
						</tr>
						<tr>
							<td>
								<a href=""> Deportes</a>
							</td>
						</tr>
						</table>
					</div>
				</div>
				<br>
				<br>
				
				<?php
				
         $query="SELECT imagen,titulo,subtitulo FROM pricipal where categoria='eventos' ORDER by id desc LIMIT 2";
          
         $resultado=$con->query($query);  
         $con ->query("SET NAMES 'utf8'");//sentencia de reconocimiento de acentos y ñ
         
         $i = 0; 
         ?>
				<div class="well">
					<h4 style="text-align:center;">Noticias Destacadas</h4>
					<div class="row">
						<div class="col-lg-12">
							 <?php 
                     while($row = mysqli_fetch_assoc($resultado)){ 
                     ?>
							<div class="cuadro_intro_hover " style="background-color:#000000;">
								<p style="text-align:center; margin-top:20px;">
									<img src="<?php echo $row['imagen']?>" class="img-responsive" alt="">
								</p>
								<div class="caption">
									<div class="blur">
									</div>
									<div class="caption-text">
										<h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;"><?php echo $row['titulo']?></h3>
										<p>
                                          <?php echo $row['subtitulo']?>
										</p>
									</div>
								</div>
							</div>
							  <?php } ?> 
						</div>
					</div>
				</div>
			</div>
		</div>
		</aside>
	</div>
</div>

<div style="padding:0; margin-top:105px;"></div>
<!--Inicio del section -->
<?php
         include_once 'php/footer.php';
         
         ?>
</body>
</html>