-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-06-2019 a las 23:25:32
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `preparatoria_128`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pricipal`
--

CREATE TABLE `pricipal` (
  `id` int(11) NOT NULL,
  `parrafo` longtext COLLATE utf8_spanish_ci NOT NULL,
  `introduccion` longtext COLLATE utf8_spanish_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `imagen` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `calificaciones` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `categoria` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `quienes_somos` longtext COLLATE utf8_spanish_ci NOT NULL,
  `mision` longtext COLLATE utf8_spanish_ci NOT NULL,
  `vision` longtext COLLATE utf8_spanish_ci NOT NULL,
  `oferta_educativa` longtext COLLATE utf8_spanish_ci NOT NULL,
  `inscripciones` longtext COLLATE utf8_spanish_ci NOT NULL,
  `videos` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `archivos` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pricipal`
--

INSERT INTO `pricipal` (`id`, `parrafo`, `introduccion`, `titulo`, `subtitulo`, `fecha`, `imagen`, `calificaciones`, `categoria`, `quienes_somos`, `mision`, `vision`, `oferta_educativa`, `inscripciones`, `videos`, `archivos`) VALUES
(1, 'Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n', '', 'Lista de alumnos aceptados del examen COMIPEMS', 'Dia de Inscripciones', '2016-07-13', 'img/slider.jpg', '', 'noticias', 'Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n\n', '\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '\r\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\r\n\r\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '\r\nPrimer Ingreso\r\nDia 26 de Agosto del 2015\r\n\r\nHorario de 15:30 - 16:50 hrs\r\n\r\n Para los Semestres I,II,III\r\nDia 26 de Agosto del 2015\r\n\r\nEn un horario de 15:30 - 16:50 hrs\r\n\r\n Para los Semestres IV,V,VI\r\nDia 26 de Agosto del 2015\r\n\r\nEn un horario de 15:30 - 16:50 hrs', '', ''),
(2, 'Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '', 'Lista de alumnos aceptados del examen COMIPEMS', 'ghfhgfhgf', '2016-07-27', 'img/slider1.jpg', 'hjgjhgj', 'noticias', '', '', '', '', '', '', ''),
(3, 'Ser una institución de nivel superior reconocida po.r su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '', 'Lista de alumnos aceptados del examen COMIPEMS', 'fds', '2016-07-18', 'img/slider2.jpg', 'sd', 'noticias', '', '', '', '', '', '', ''),
(4, ' Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '', 'Lista de alumnos aceptados del examen COMIPEMS', 'czxcxzcxz', '2016-07-30', 'img/slider3.jpg', '', 'noticias', '', '', '', '', '', '', ''),
(5, ' Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '', 'Lista de alumnos aceptados del examen COMIPEMS', 'fdsfds', '2016-08-16', 'img/slider4.jpg', '', 'noticias', '', '', '', '', '', '', ''),
(6, '', '', 'Dia de Muertos', '											 2 de Noviembre 2015 a las 15:00 en la Institución', '2016-07-21', 'img/entrega.jpg', '', 'eventos', '', '', '', '', '', '', ''),
(7, '', '', 'Dia del Maestro', '											 2 de Noviembre 2015 a las 15:00 en la Institución', '2016-07-29', 'img/docente.jpg', '', 'eventos', '', '', '', '', '', '', ''),
(8, '', '', 'Programa de Becas', 'Lunes  2 de octubre a las 15:00', '2016-07-21', 'img/becas.jpg', '', 'eventos', '', '', '', '', '', '', ''),
(9, 'Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo \"Contenido aquí, contenido aquí\". Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de \"Lorem Ipsum\" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).\n\n', '', 'Semana de la Ciencia y Tecnología ', 'dasdsa', '2016-08-17', 'img/', '', 'Semana de la  Ciencia', '', '', '', '', '', 'https://www.youtube.com/embed/O-Cekhk0KVE', ''),
(10, 'Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo \"Contenido aquí, contenido aquí\". Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de \"Lorem Ipsum\" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).\n\n', '', '2 de Octubre no se olvida', 'gfdgfdg', '2016-08-10', 'f', 'fdsfd', '2 de octubre no se olvida ', '', '', '', '', '', 'https://www.youtube.com/embed/VtLDu9uihgg', ''),
(11, 'Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo \"Contenido aquí, contenido aquí\". Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de \"Lorem Ipsum\" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).\n\n', '', 'Muestra Gastronómica', 'fdfd', '2016-08-11', '', '', 'muestra gastronomica', '', '', '', '', '', 'https://www.youtube.com/embed/f_R5RQZ_-G4', ''),
(12, '\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '', 'Visión', 'dsdsd', '2016-08-12', '', '', 'vision', '', '', '', '', '', '', ''),
(13, '\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n\nSer una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.\n\n', '', 'Misión', 'hgfhgf', '2016-08-11', '', '', 'mision', '', '', '', '', '', '', ''),
(14, 'Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.  Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '', 'Quiénes Somos', '', '2016-08-09', 'img/slider2.jpg', '', 'quienes somos', '', '', '', '', '', '', ''),
(15, 'Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.  Ser una institución de nivel superior reconocida por su nivel educativo, que forme a sus egresados con una actitud humana; De liderazgo y calidad, que sean capaces de hacer aportaciones tecnológicas, innovadoras y vanguardistas tanto del sector productivo como el de servicios.', '', 'Reinscripciones', 'tyiuyiuy', '2016-08-11', 'img/slider2.jpg', 'gjgjg', 'reinscripciones', '', '', '', '', '', '', ''),
(16, 'Les pedimos a los alumnos estén pendiente de las fechas por materia ya que serán publicadas en la escuela y la solicitud de regularización es la siguiente:\n', '\"Es importante que la solicitud este firmada por el orientador y el padre o tutor ya que no serán recibida por la administración escolar\"  La solicitud se tiene que entregar junto con los bauchers de pago en original y una copia.  Las guías se descargaran en esta sección. ', 'Solicitud de regularización', 'Guías de estudio para presentar los periodos de regularización', '2016-08-16', 'img/slider2.jpg', '', 'guias', '', '', '', '', '', '', 'archivos/requsitos.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `configcontrasena` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `privilegio` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `email`, `contrasena`, `configcontrasena`, `privilegio`, `url`) VALUES
(1, 'Mauricio', 'Montes', 'marmol.signe@gmail.com', 'marmol87', 'marmol87', 'Administrador', 'img/fotos_user/1436801897_490586_1436806658_album_normal.jpg'),
(2, 'Ivan', 'de la rosa', 'ivan_edmundo@hotmail.com', '2013370040', '2013370040', 'Colaborador', 'img/fotos_user/ivan.jpeg'),
(3, 'Oscar', 'Nolasco', 'oscar_anolasco@hotmail.com', 'OaNG1609', 'OaNG1609', 'Editor', 'img/fotos_user/oscar.jpeg'),
(4, 'Erick', 'Garcia', 'ei.garcia.rodriguez@gmail.com', 'lego_3.6!', 'lego_3.6!', 'Colaborador', 'img/fotos_user/IMG_6524.JPG'),
(5, 'Cesar', 'Bazan', 'bazancesar11@gmail.com', '12345', '12345', 'Administrador', 'img/fotos_user/cesar.png'),
(6, 'Aldo', 'Cabrera', 'aldo_cabrera2006@hotmail.com', 'aldoCRamos1991', 'aldoCRamos1991', 'Administrador', 'img/fotos_user/IMG_6527.JPG'),
(7, 'Marco', 'Morales', 'marco.antonio.morales.quintero@gmail.com', 'elotroyo', 'elotroyo', 'Colaborador', 'img/fotos_user/13518009_1102652759805915_1665821272_o.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pricipal`
--
ALTER TABLE `pricipal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pricipal`
--
ALTER TABLE `pricipal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
