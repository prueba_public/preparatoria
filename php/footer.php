<footer>
<div class="container">
	<div class="col-md-4">
		<br/>
		<hr class="line"/>
	</div>
	<div class="col-md-4 text-center">
		<img class="img-a " src="img/epo.png"/>
	</div>
	<div class="">
		<br/>
		<hr class="line"/>
	</div>
	<br>
	<div class="form-group">
		<h5 class="text-center"> Preparatoria General Francisco Villa N°128</h5>
		<address class="text-center">
		Calle. José María Morelos Lote 1, Extensión 21, Ecatepec De Morelos, Ecatepec De Morelos, México 55520<br>
		 Codigo Postal: 55520<br/>
		<span class="fa fa-phone"></span> (55) 56996845 </address>
	</div>
	<hr>
	<div class="text-center center-block">
		<h3 class="txt-railway">Siguenos en</h3>
		<br/>
		<ul class="social-network social-circle">
			<li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
		</ul>
	</div>
	<hr>
</div>
</footer>