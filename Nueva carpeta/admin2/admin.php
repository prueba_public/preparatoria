<?php
   require_once("php/sesion.class.php");
   require_once("php/config.php");
   
   $sesion = new sesion();
   $email = $sesion->get("email");
   $nombre = $sesion->get_datos("nombre");
   $privilegio = $sesion->get_privilegio("privilegio");
   $apellidos = $sesion-> get_apellidos("apellidos");
   $imagen_perfil = $sesion-> get_imagenperfil("url");
   if( $email == false )
   {	
   	header("Location: index.php");		
   }
   
   else 
   {
   $email=$_SESSION['email'];
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Panel Administrador</title>
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="css/AdminLTE.min.css">
      <link rel="stylesheet" href="css/_all-skins.min.css">
      <link rel="stylesheet" href="css/sweetalert.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
      <script src="js/sweetalert.min.js"></script>
   </head>
   <body class=" skin-blue" style="background-color:#ecf0f5;">
      <div class="wrapper">
      <header class="main-header" >
         <a href="#" class="logo" style="background-color:#3b5998;">
         <span class="logo-mini"><b>Bienvenidos</b></span>
         <span class="logo-lg"><b>Bienvenidos</b></span>
         </a>
         <nav class="navbar navbar-static-top" role="navigation" style="background-color:#3b5998;">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            </a>
            <div class="navbar-custom-menu">
               <ul class="nav navbar-nav" style="text-decoration:none;">
                  <li class="dropdown user user-menu">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     <small><?php echo $sesion->get("email"); ?></small>
                     </a>
                     <ul class="dropdown-menu" >
                        <li class="user-header" style="background-color:#3b5998;">
                           <img src="<?php echo $imagen_perfil; ?>" class="img-circle" alt="User Image">
                           <p>
                              <?php echo $privilegio; ?>
                              <small><?php echo $sesion->get("email"); ?></small>
                           </p>
                        </li>
                        <li class="user-footer">
                           <div class="pull-left">
                              <a href="#" class="btn btn-default btn-flat">Perfil</a>
                           </div>
                           <div class="pull-right">
                              <a href="php/cerrarsesion.php" class="btn btn-default btn-flat">Cerrar Sesión</a>
                           </div>
                        </li>
                     </ul>
                  </li>
               </ul>
            </div>
         </nav>
      </header>
      <aside class="main-sidebar" style="background-color:;">
         <section class="sidebar">
            <div class="user-panel">
               <div class="pull-left image">
                  <br>
                  <br>
               </div>
               <div class="pull-left info">
                  <p><?php echo $nombre; ?> <?php echo $apellidos; ?></p>
                  <a href="#"><i class="fa fa-circle text-success"></i>Conectado</a>
               </div>
            </div>
            <form action="#" method="get" class="sidebar-form">
               <div class="input-group">
                  <input type="text" name="q" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span>
               </div>
            </form>
            <?php
               if($privilegio=="Administrador"){
               
               ?>
            <ul class="sidebar-menu">
               <li class="header">Menu de Operaciones</li>
               <li class="active treeview">
                  <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Articulos</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                     <li class="active"><a href="insertar.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Crear Articulo admin</a></li>
                     <li class="active"><a href="lista.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Editar Articulo admin</a></li>
                     <li class="active"><a href="lista2.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Eliminar Articulo admin</a></li>
                     <li class="active"><a href="lista_arti.php"><i class="fa fa-circle-o"></i>Editar todos los Articulos</a></li>
                     <li class="active"><a href="lista_arti_delete.php"><i class="fa fa-circle-o"></i>Eliminar todos los Articulos</a></li>
                  </ul>
               </li>
               <li class="treeview">
                  <a href="#">
                  <i class="fa fa-calendar"></i>
                  <span>Convocatorias</span>
                  <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                     <li><a href="insertar_convoca.php"><i class="fa fa-circle-o"></i> Crear  Convocatoria</a></li>
                     <li><a href="lista_convoca.php"><i class="fa fa-circle-o"></i> Editar convocatoria</a></li>
                     <li><a href="lista_convoca_delete.php"><i class="fa fa-circle-o"></i> Eliminar convocatoria</a></li>
                  </ul>
               </li>
               <li class="treeview">
                  <a href="#">
                  <i class="glyphicon glyphicon-file"></i> <span>Ediciones anteriores</span>
                  <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                     <li><a href="insertar_edicion.php"><i class="fa fa-circle-o"></i> Crear una Edicion</a></li>
                     <li><a href="lista_edicion.php"><i class="fa fa-circle-o"></i> Editar Edicion</a></li>
                     <li><a href="lista_edicion_delete.php"><i class="fa fa-circle-o"></i> Eliminar Edicion</a></li>
                  </ul>
               </li>
               <li class="treeview">
                  <a href="#">
                  <i class="ion ion-person-add"></i>
                  <span>Registros de Usuarios</span><i class="fa fa-angle-left pull-right"></i>                
                  </a>
                  <ul class="treeview-menu">
                     <li class="active"><a href="insertar_user.php"><i class="fa fa-circle-o"></i> Crear Usuario</a></li>
                     <li class="active"><a href="lista_user.php"><i class="fa fa-circle-o"></i> Editar Usuario</a></li>
                     <li class="active"><a href="elimina_user.php"><i class="fa fa-circle-o"></i> Eliminar Usuario</a></li>
                  </ul>
               </li>
            </ul>
            <?php
               }
               ?>
            <?php
               if($privilegio=="Editor"){
               
               ?>
            <ul class="sidebar-menu">
               <li class="header">Menu de Operaciones</li>
               <li class="active treeview">
                  <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Articulos</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                     <li class="active"><a href="lista.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Editar Articulo</a></li>
                     <li class="active"><a href="lista2.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Eliminar Articulo</a></li>
                  </ul>
               </li>
               <li class="treeview">
                  <a href="#">
                  <i class="fa fa-calendar"></i>
                  <span>Convocatorias</span>
                  <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                     <li><a href="lista_convoca.php"><i class="fa fa-circle-o"></i> Editar convocatoria</a></li>
                     <li><a href="lista_convoca_delete.php"><i class="fa fa-circle-o"></i> Eliminar convocatoria</a></li>
                  </ul>
               </li>
               <li class="treeview">
                  <a href="#">
                  <i class="glyphicon glyphicon-file"></i> <span>Ediciones anteriores</span>
                  <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                     <li><a href="lista_edicion.php"><i class="fa fa-circle-o"></i> Editar Edicion</a></li>
                     <li><a href="lista_edicion_delete.php"><i class="fa fa-circle-o"></i> Eliminar Edicion</a></li>
                  </ul>
               </li>
            </ul>
            <?php
               $query="SELECT id, titulo, subtitulo, autor,imagen FROM articulos  WHERE tipo_usuario='Editor'";
               $resultado=$con->query($query);  
               $i = 0; 
               ?>
            <?php
               }
               ?>
            <?php
               if($privilegio=="Colaborador"){
               
               
               ?>
            <ul class="sidebar-menu">
               <li class="header">Menu de Operaciones</li>
               <li class="active treeview">
                  <a href="#">
                  <i class="fa fa-dashboard"></i> <span>Articulos</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                     <li class="active"><a href="insertar.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Crear Articulo</a></li>
                     <li class="active"><a href="lista.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Editar Articulo</a></li>
                     <!-- <li class="active"><a href="lista2.php?privilegio=<?php echo $privilegio; ?>"><i class="fa fa-circle-o"></i>Eliminar Articulo</a></li>-->
                  </ul>
               </li>
            </ul>
            <?php
               $query="SELECT id, titulo, subtitulo, autor FROM articulos  WHERE tipo_usuario='Colaborador'";
               $resultado=$con->query($query);  
               $i = 0; 
               ?>
            <?php
               }
               ?>
         </section>
      </aside>
      <div class="content-wrapper">
         <section class="content-header">
            <h1>
               Administrar
               <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
               <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
               <li class="active">Administrador</li>
            </ol>
         </section>
         <section class="content">
         <?php
            if($privilegio=="Administrador"){
            ?>
         <div class="col-md-8">
            <div class="box box-info">
               <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                  <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                     </button>
                     <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
               </div>
               <div class="box-body">
                  <div class="table-responsive">
                     <table class="table no-margin">
                        <thead>
                           <tr>
                              <th><a href="pages/examples/invoice.html">N° Articulo</a></th>
                              <th>Titulo</th>
                              <th>SubTitulo</th>
                              <th>Autor</th>
                              <th>Actualizar</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 
                              while($row = mysqli_fetch_assoc($resultado)){ 
                              
                              
                              ?>
                           <tr>
                              <td><?php echo $row['id']?></td>
                              <td><?php echo $row['titulo']?></td>
                              <td><?php echo $row['subtitulo']?></td>
                              <td><?php echo $row['autor']?></td>
                              <td>
                                 <p data-placement="top" data-toggle="tooltip" title="Actualizar"><a class="btn btn-primary btn-xs" href="actualiza.php?id=<?php echo $row['id'];?>"><span class="glyphicon glyphicon-refresh"></span></a></p>
                              </td>
                           </tr>
                           <?php } ?> 
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="box box-widget widget-user">
               <div class="widget-user-header bg-black" style="background: url('img/fondo/espacio.jpg') center center;">
                  <h3 class="widget-user-username"><?php echo $nombre; ?> <?php echo $apellidos; ?> </h3>
               </div>
               <div class="widget-user-image">
                  <img class="img-circle" src="<?php echo $imagen_perfil; ?>" alt="User Avatar">
               </div>
               <div class="box-footer">
                  <div class="row">
                     <div class="col-sm-4 border-right">
                        <div class="description-block">
                        </div>
                        <!-- /.description-block -->
                     </div>
                     <!-- /.col -->
                     <div class="col-sm-4 border-right">
                        <div class="description-block">
                           <h5 class="description-header"><?php echo $privilegio; ?></h5>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            }
            ?>
         <?php
            if($privilegio=="Editor"){
            
            
            
            
            ?>
         <div class="col-md-8">
            <div class="box box-info">
               <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                  <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                     </button>
                     <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
               </div>
               <div class="box-body">
                  <div class="table-responsive">
                     <table class="table no-margin">
                        <thead>
                           <tr>
                              <th><a href="pages/examples/invoice.html">N° Articulo</a></th>
                              <th>Titulo</th>
                              <th>SubTitulo</th>
                              <th>Autor</th>
                              <th>Actualizar</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 
                              while($row = mysqli_fetch_assoc($resultado)){ 
                              
                              
                              ?>
                           <tr>
                              <td><?php echo $row['id']?></td>
                              <td><?php echo $row['titulo']?></td>
                              <td><?php echo $row['subtitulo']?></td>
                              <td><?php echo $row['autor']?></td>
                              <td>
                                 <p data-placement="top" data-toggle="tooltip" title="Actualizar"><a class="btn btn-primary btn-xs" href="actualiza.php?id=<?php echo $row['id'];?>"><span class="glyphicon glyphicon-refresh"></span></a></p>
                              </td>
                           </tr>
                           <?php } ?> 
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="box box-widget widget-user">
               <div class="widget-user-header bg-black" style="background: url('img/fondo/espacio.jpg') center center;">
                  <h3 class="widget-user-username"><?php echo $nombre; ?> <?php echo $apellidos; ?> </h3>
               </div>
               <div class="widget-user-image">
                  <img class="img-circle" src="<?php echo $imagen_perfil; ?>" alt="User Avatar">
               </div>
               <div class="box-footer">
                  <div class="row">
                     <div class="col-sm-4 border-right">
                        <div class="description-block">
                        </div>
                     </div>
                     <div class="col-sm-4 border-right">
                        <div class="description-block">
                           <h5 class="description-header"><?php echo $privilegio; ?></h5>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            }
            if($privilegio=="Colaborador"){
            
            ?>
         <div class="col-md-8">
            <div class="box box-info">
               <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                  <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                     </button>
                     <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
               </div>
               <div class="box-body">
                  <div class="table-responsive">
                     <table class="table no-margin">
                        <thead>
                           <tr>
                              <th><a href="pages/examples/invoice.html">N° Articulo</a></th>
                              <th>Titulo</th>
                              <th>SubTitulo</th>
                              <th>Autor</th>
                              <th>Actualizar</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 
                              while($row = mysqli_fetch_assoc($resultado)){ 
                              
                              
                              ?>
                           <tr>
                              <td><?php echo $row['id']?></td>
                              <td><?php echo $row['titulo']?></td>
                              <td><?php echo $row['subtitulo']?></td>
                              <td><?php echo $row['autor']?></td>
                              <td>
                                 <p data-placement="top" data-toggle="tooltip" title="Actualizar"><a class="btn btn-primary btn-xs" href="actualiza.php?id=<?php echo $row['id'];?>"><span class="glyphicon glyphicon-refresh"></span></a></p>
                              </td>
                           </tr>
                           <?php } ?> 
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="box box-widget widget-user">
               <div class="widget-user-header bg-black" style="background: url('img/fondo/espacio.jpg') center center;">
                  <h3 class="widget-user-username"><?php echo $nombre; ?> <?php echo $apellidos; ?> </h3>
               </div>
               <div class="widget-user-image">
                  <img class="img-circle" src="<?php echo $imagen_perfil; ?>" alt="User Avatar">
               </div>
               <div class="box-footer">
                  <div class="row">
                     <div class="col-sm-4 border-right">
                        <div class="description-block">
                        </div>
                     </div>
                     <div class="col-sm-4 border-right">
                        <div class="description-block">
                           <h5 class="description-header"><?php echo $privilegio; ?></h5>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php 
         }
         }	
         ?>
      <?php 
         if(isset($_GET["id"]) && !empty($_GET["id"])){
         	if($_GET["id"] == "correcto"){
         		echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \" Login Correcto\", \"success\");});</script>";
         		
         	}else if($_GET["id"] == "incorrecto"){
         		echo "
         		<script>jQuery(function(){swal(\"¡Error!\", \"Error Datos Incorrectos\", \"error\");});</script>
         		";
         	}
         	
         	else if($_GET["id"] == "articulo_correcto"){
         		echo "
         		<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Articulo Insertado Correctamente\", \"success\");});</script>
         		";
         	}
         else if($_GET["id"] == "articulo_incorrecto"){
         		echo "
         		<script>jQuery(function(){swal(\"¡Error!\", \"Error al insertar articulo\", \"error\");});</script>
         		";
         	}
         	else if($_GET["id"] == "articulo_actualizado"){
         		echo "
         		<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Articulo Actualizado Correctamente\", \"success\");});</script>
         		";
         	}
         else if($_GET["id"] == "articulo_no_actualizado"){
         		echo "
         		<script>jQuery(function(){swal(\"¡Error!\", \"Error al actulizar articulo\", \"error\");});</script>
         		";
         	}
         	else if($_GET["id"] == "articulo_eliminado"){
         		echo "
         		<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Articulo Eliminado Correctamente\", \"success\");});</script>
         		";
         	}
         else if($_GET["id"] == "articulo_no_eliminado"){
         		echo "
         		<script>jQuery(function(){swal(\"¡Error!\", \"Error al Eliminar articulo\", \"error\");});</script>
         		";
         	}
         }
         ?>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/app.min.js"></script>
   </body>
</html>
