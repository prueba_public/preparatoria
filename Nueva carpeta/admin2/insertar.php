<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}

  $error = $_GET['error'];
  $privilegio = $_GET['privilegio'];
	?>

<?php include(''); 
      

?>

 <h1>Crear Articulo de la Revista AEM</h1>
 <?php
 if($error == "imagen"){
 ?>
 <br>
 <h2 style="color:red;text-align:center;">Error al insertar la imagen comprueba que la extension sea .jpg o .png y el archivo pese no mas de 800kb </h2>
 <?php
 }else if($error == "subida"){

 ?>
<h2 style="color:red;text-align:center;">No se inserto en base de datos Intentalo de nuevo </h2>
 <?php
}
 ?>

<div class="container">
  <div class="form-group">
  
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Ir al Panel de Adminsitrador</a></li>
   
  </ul>
  <br><br>
</div></div>
     <div class="form-group">
	  <section id="formulario">
      <div class="container" id="log">
         <form action="php/inserta.php" method="post" enctype="multipart/form-data">
            <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Titulo del Articulo</label>
                  <input type="text" class="form-control"  id="titulo" name="titulo"  required >
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label>Subtitulo</label>
                  <input type="text" class="form-control" id="Subtitulo" name="subtitulo">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Descripción</label>
                  <textarea class="form-control" id="descripcion1" name="descripcion" cols="30" rows="8"></textarea> 
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			<div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Articulo</label>
                 <textarea name="articulo" id="articulo" cols="30" rows="10"></textarea>
                 <script type="text/javascript">
		             CKEDITOR.replace('articulo', {
                       extraPlugins: 'imageuploader'
                     });

		
		         </script>
               </div>
			   
            </div>
			 <!--<div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Nombre del Autor</label>
                  <input type="text" class="form-control" id="autor" name="autor" required>
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>-->
			  <div class="row control-group">
               <div class="form-group col-xs-6">
                 <label>Nombre del Autor</label>
                  <input type="text" class="form-control" id="autor" name="autor" required>
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label>Tipo de Usuario</label>
                  <input type="text" class="form-control" placeholder="tipo_usuario" id="tipo_usuario" name="tipo_usuario" value="<?php echo $privilegio;?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>URL Video</label>
                  <input type="url" class="form-control" placeholder="URL" id="video" name="video">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <!-- <div class="form-group col-xs-6 ">
                  <label>Galeria</label>
                  <input type="number" class="form-control" placeholder="Galeria" id="galeria" name="galeria">
                  <p class="help-block text-danger">
                  </p>
               </div> -->
               <div class="form-group col-xs-6 ">
                  <label>Tags</label>
                  <input type="text" class="form-control" placeholder="tags" id="tags" name="tags"  required title="Tags">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group" >
               <div class="form-group col-xs-6 ">
                  <label for="categoria">Categoria</label>
                  <select id="categoria" class="form-control"  name="categoria" required >
				              <option></option>
					            <option value="Articulos">Articulos</option>
                      <option value="Secciones">Secciones</option>
					            <option value="Noticias">Noticias</option>
                  </select>
               </div>
               <div class="form-group col-xs-6 " id="secciones">
                  <label for="seccion">Sección</label>
                  <select class="form-control" id="seccion"  name="seccion">
                     <option></option>
                    <option value="¿Quien es Quién?">¿Quien es Quién?</option>
                     <option value="Mirando las Estrellas">Mirando las Estrellas</option>
                     <option value="Recordando a...">Recordando a...</option>
                     <option value="Recomendación del Mes">Recomendación del Mes</option>
                     <option value="Jóvenes Talento">Jóvenes Talento</option>
                     <option value="¿Sabias que..?">¿Sabias que...?</option>
                     <option value="Espacio Geek">Espacio Geek</option>
                     <option value="Espacio de los niños">Espacio de lo niños</option>
                  </select>
               </div>
            </div>
            
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Numero de Publicación</label>
                  <input type="number" class="form-control" placeholder="N° Publicación" id="publicacion" name="publicacion" required title="Publicación">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>Columna/art.</label>
                  <input type="text" class="form-control" placeholder="Columna/Articulo" id="columna_articulo" name="columna_articulo"  title="columna_articulo">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Pie de Imagen</label>
                  <input type="text" class="form-control" placeholder="Pie de Imagen" id="pie_de_imagen"  name="pie_de_imagen"  title="Pie de Imagen">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>Pie de Video</label>
                  <input type="text" class="form-control" placeholder="Pie de Video" id="pie_de_video" name="pie_de_video"   title="Pie de Video">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			
            <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Fecha</label>
                  <input type="date" class="form-control" placeholder="Fecha" id="fecha"  name="fecha" required title="Fecha">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>Referencias</label>
                  <input type="text" class="form-control" placeholder="Referencia" id="referencia" name="referencia">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			<div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
			   	<div class="form-group">
				 <label>Selecciona una magen</label>
                 </div>
				 <input id="image" type="file" name="imagen"  REQUIRED>
               </div>
            </div>
            <div class="row control-group" style="text-align:right;">
              <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Crear Articulo">
               <input type="reset" class="btn btn-danger " name="cancel"  value="Cancelar">
            </div>
         </form>
      </div>
	  </section>
	  </div>
      <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Datos Insertados Correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"No se insertarón Datos\", \"error\");});</script>
				";
			}
		}
	 ?>
	  <?php
      include_once '';
     ?>
     <script>

      $(document).ready(function(){


        $('#categoria').change(function(){
         var categoria = $(this).val();
            switch(categoria){
               case "Articulos":
                  $('#secciones').hide();
                  break;
               case "Secciones":
                  $('#secciones').show();
                  break;
               case "Noticias":
                  $('#secciones').hide();
                  break;
               }
        })
      })
     </script>
   </body>
</html>