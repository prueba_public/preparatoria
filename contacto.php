<!DOCTYPE html>
<html lang="en">
<?php
      include_once 'php/head.php';
      require 'admin/php/config.php';
	  
      ?>

<body>


<?php
      include_once 'php/header.php';
      
	  
      ?>
<br>
<br>

<section>
<div class="container">
	<div class="col-md-4">
		<br/>
		<hr class="line"/>
	</div>
	
	<div class="col-md-4 text-center">
		<h1 id="titulo"><em>Datos de Contacto</em></h1>
	</div>
	
	<div>
		<br/>
		<hr class="line">
	</div>
 
</div>

</section>
 <section id="section_contacto">
         <div class="container">
            <div class="row" style="padding: 80px 0;">
               <div class="col-md-7">
			   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3759.8138421150984!2d-99.07546868509147!3d19.549604186817966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f094c0000001%3A0xd54af3520d97490d!2sESCUELA+PREPARATORIA+OFICIAL+128!5e0!3m2!1ses!2smx!4v1471823238868" width="100%" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
			   
               
				 <strong>
                     <p class="direccion_padding">Dirección:</p>
                  </strong>
                  <p class="parrafo_contacto">
				  José María Morelos Lote 1, Extensión 21,
				  Ecatepec De Morelos,
				  Ecatepec De Morelos, México 55520
				  </p>
				   <strong>
                     <p class="direccion_padding">Teléfono:</p>
                  </strong>
                  <p  class="parrafo_contacto">(55) 56996845</p>
 
				  <strong>
                     <p class="correo_padding">Correo Electrónico:</p>
                  </strong>
                  <p>
                    epo128@hotmail.com
                  </p>
               </div>
               <div class="col-md-5" id="contacto_fondo">
                  <h3 class="encabezado"><strong>Contacto</strong></h3>
                  <hr>
                  <form method="POST" action="php/mail2.php">
                     <div class="form-group">
                        <label for="nombreCompleto">Nombre completo:</label>
                        <input type="text" class="form-control" name="nombre" placeholder="Escribe tu nombre" id="nombreContacto" required title="Nombre completo"/>
                     </div>
                     <div class="form-group">
                        <label for="correoElectronico">Correo electrónico:</label>
                        <input type="email" class="form-control" name="email" placeholder="Escribe tu correo" id="correoContacto" required title="Email">
                     </div>
                     <div class="form-group">
                        <label for="asunto">Asunto:</label>
                        <textarea class="form-control" name="mensaje" cols="40" rows="5" placeholder="Escribe tu mensaje" id="asuntoContacto" required title="Escribe un mensaje"></textarea>   
                     </div>
					 <div>
		            <input type="submit" name="enviar" value="Enviar" class="btn btn-primary">
		            </div>
                  </form>

               </div>
			   
            </div>
			
         </div>
      </section>

<?php
         include_once 'php/footer.php';
         
         ?>
</body>
</html>