<!DOCTYPE html>
<html lang="en">
   <head>
      <title>AEM-Forms</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/formulario.css">
	  <link href="css/sweetalert.css" rel="stylesheet">
	  <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/articulo.css">
	  <link rel="shortcut icon" href="img/favicon.ico">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
	  <script src="ckeditor/ckeditor.js"></script>
	  <script src="js/sweetalert.min.js"></script>
	  <script src="js/valida.js"></script>
	  <script>
		
	</script>
	<style>
	
	</style>
   </head>
   <body>
      <header class="container-fluid nopad">
         <div class="container">
            <div class="row sipad">
               <div class="col-md-6 "><img class="logo" src="img/logo.png"></div>
               <div class="col-md-3 col-sm-6 col-xs-12">
                  <img class="logos" src="img/stcaem.png">
               </div>
               <div class="col-md-3 col-sm-6 col-xs-12 nopad">
                  <div id="redesBuscar">
                     <div id="redes">
                        <i class="fa fa-youtube-square"></i>
                        <i class="fa fa-twitter-square"></i>
                        <i class="fa fa-facebook-square"></i>
                        <i class="fa fa-video-camera"></i>
                     </div>
                     <div id="buscar">
                        <span class="fa fa-search"></span>
                        <input type="text" placeholder="buscar" class="buscar" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>