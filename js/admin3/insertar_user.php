<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>
<?php include('php/header.php'); 
?>
<h1>Registar Usuarios</h1>
<div class="container">
  <div class="form-group">
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Ir al Panel de Adminsitrador</a></li>
  </ul>
</div></div>
      <div class="form-group">
      <div class="container" id="log">
         <form action="php/mail2.php" method="post" enctype="multipart/form-data">
		   <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
				 <img id="ver_image" src="img/img.jpg">
				 <input id="image" type="file" name="imagen"  REQUIRED  onchange="PreviewImage();" >
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Nombre Completo:</label>
                  <input type="text" class="form-control" placeholder="Nombre:"  name="nombre"  required title="Nombre">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                 <label>Apellido Paterno:</label>
                  <input type="text" class="form-control" placeholder="Apellidos:"  name="apellidos" required title="Apellidos">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label for="privilegio">Tipo de Usuario:</label>
                  <select class="form-control" name="privilegio" required title="Tipo de Privilegio">
                     <option></option>
                     <option>Administrador</option>
                     <option>Editor</option>
                     <option>Colaborador</option>
                  </select>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Correo Electronico:</label>
                  <input type="email" class="form-control" placeholder="E-mail:"  name="email" required title="Email">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                 <label>Contraseña:</label>
                  <input type="password" class="form-control" id="contrasena" placeholder="Password:"  name="contrasena" required title="password">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label>Confirmar Contraseña:</label>
                  <input type="password" class="form-control" id="configcontrasena" placeholder="Repetir Password:"  name="configcontrasena" required title="confirmar password">
                  <p class="help-block text-danger">
                  </p>
               </div>  
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                 <label>
                  <input type="checkbox" id="mostrar_contrasena" title="clic para mostrar contraseña">
                  <span>  Ver Contraseña </span>
                  </label>
               </div>
            </div>
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Registrar">
            <input type="reset" class="btn btn-danger " name="cancel"  value="Cancelar">
         </form>
      </div>
      </div>
	  <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"El usuario se registro correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"El usuario ya existe\", \"error\");});</script>
				";
			}
			else if($_GET["id"] == "registrado"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"Erorr el usuario ya existe\", \"error\");});</script>
				";
			}else if($_GET["id"] == "password"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"El password no coincide\", \"error\");});</script>
				";
			}
			
			
			
		}
	 ?>
    <?php
      include_once 'php/footer.php';
     ?>
	  <script type="text/javascript">

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("ver_image").src = oFREvent.target.result;
        };
    };

</script>
   </body>
</html>