<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>
<?php include('php/header.php');?>
   <?php
      require_once ('php/config.php');
         $id= $_GET['id'];
         $sql = "SELECT * FROM usuarios where id=$id";
         $result = $con->query($sql);
if ($result) {
        
              // obtine los valores por medio del id de las columnas de la tabla 
              while($row = $result->fetch_assoc()) {
?>
<h1>Editar Usuario</h1>
<div class="container">
  <div class="form-group">
  <ul class="controls">
    <li class="control-prev"><a href="lista_user.php">&lt; Seguir Actualizando</a></li>
  </ul>
</div></div>
      <div class="form-group">
      <div class="container" id="log">
         <form action="php/actualizar_user.php" method="post" enctype="multipart/form-data">
		 <input type="hidden" name="id"  value="<?php echo $id;?>">
		  <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
				 <img id="ver_image" src="<?php echo $row['url']?>">
				 <input id="image" type="file" name="imagen"   onchange="PreviewImage();">
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Nombre Completo:</label>
                  <input type="text" class="form-control" placeholder="Name:"  name="nombre"  required title="Nombre" value="<?php echo $row['nombre']?>" >
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                 <label>Apellido Paterno:</label>
                  <input type="text" class="form-control" placeholder="Apellidos:"  name="apellidos" required title="Apellidos" value="<?php echo $row['apellidos']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label for="privilegio">Tipo de Usuario:</label>
                  <select class="form-control" name="privilegio" required title="opcion">
                     <option><?php echo $row['privilegio']?></option>
					 <option>Administrador</option>
                     <option>Editor</option>
                     <option>Colaborador</option>
                  </select>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Correo Electronico:</label>
                  <input type="email" class="form-control" placeholder="E-mail:"  name="email" required title="Email" value="<?php echo $row['email']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                 <label>Contraseña:</label>
                  <input type="password" class="form-control" id="contrasena" placeholder="Password:"  name="contrasena" required title="password" value="<?php echo $row['contrasena']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label>Confirmar Contraseña:</label>
                  <input type="password" class="form-control" id="configcontrasena" placeholder="Repetir Password:"  name="configcontrasena" required title="confirmar password" value="<?php echo $row['configcontrasena']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                 <label>
                  <input type="checkbox" id="mostrar_contrasena" title="clic para mostrar contraseña">
                  <span>  Ver Contraseña </span>
                  </label>
               </div>
            </div>
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Editar Usuario">
			<a href="lista_user.php" class="btn btn-danger">Cancelar</a>
         </form>
		
      </div>
      </div>
	   <?php
             }

			 } 
         

         $con->close();
?>
	   <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Datos Actualizados Correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"No se Actualizaron los Datos\", \"error\");});</script>
				";
			}
		}
	 ?>
     
    <?php
      include_once 'php/footer.php';
     ?>
	  <script type="text/javascript">

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("ver_image").src = oFREvent.target.result;
        };
    };

</script>
   </body>
</html>