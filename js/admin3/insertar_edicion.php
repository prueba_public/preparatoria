<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>
<?php include('php/header.php'); 
?>
<h1>Ediciones Anteriores</h1>
<div class="container">
  <div class="form-group">
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Ir al Panel de Adminsitrador</a></li>
  </ul>
</div></div>
      <div class="form-group">
      <div class="container" id="log">
         <form action="php/inserta_edicion.php" method="post" enctype="multipart/form-data">
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Nombre de la Edición:</label>
                  <input type="text" class="form-control" placeholder="Edicion:"  name="edicion"  required title="Edicion">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                                   <label>N° de la Edición</label>
                  <input type="number" class="form-control" placeholder="N°" id="numero_edicion" name="numero_edicion">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>URL de la Edición</label>
                  <input type="url" class="form-control" placeholder="URL edicion" id="url_edicion" name="url_edicion" required title="Url de la Edicion">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                   <label>Categoria</label>
                  <input type="number" class="form-control" placeholder="Categoria" id="categoria" name="categoria">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			<div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
			   	<div class="form-group">
				 <label>Imagen de Ediciones</label>
                 </div>
				 <input id="image" type="file" name="imagen"  REQUIRED>
               </div>
            </div>
           
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Crear Edicion">
            <input type="reset" class="btn btn-danger " name="cancel"  value="Cancelar">
         </form>
      </div>
      </div>
	  <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Edicion creada correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"No se creo la Edicion\", \"error\");});</script>
				";
			}
		}
	 ?>
    <?php
      include_once 'php/footer.php';
     ?>
   </body>
</html>