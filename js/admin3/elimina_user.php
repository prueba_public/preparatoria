<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>
<?php
	require('php/config.php');
	
	$query="SELECT id, nombre, apellidos, email,privilegio FROM usuarios";
	
	$resultado=$con->query($query);
	
?>
<?php include('php/header.php'); ?>
       <h1>Eliminar Lista de Usuarios </h1>
<div class="container">
  <div class="form-group">
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Ir al Panel de Administrador</a></li>
  </ul>
</div>
</div> 
<div class="form-group">
      <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="table-responsive">
               <table id="mytable" class="table table-bordred table-striped">
                  <thead>
                     <th>N°</th>
                     <th>Nombre</th>
                     <th>Apellidos</th>
                     <th>Correo Electronico</th>
                     <th>Privilegio</th>
                  </thead>
                  <tbody>
				    <?php while($row=$resultado->fetch_assoc()){ ?>
                     <tr>
                        <td><?php echo $row['id'];?></td>
                        <td><?php echo $row['nombre'];?></td>
                        <td><?php echo $row['apellidos'];?></td>
                        <td><?php echo $row['email'];?></td>
                        <td><?php echo $row['privilegio'];?></td>
                        <td>
						</td>
                        <td>
                             <p data-placement="top" data-toggle="tooltip" title="Eliminar"><a class="btn btn-danger btn-xs" href="eliminar_user.php?id=<?php echo $row['id'];?>"><span class="glyphicon glyphicon-trash"></span></a></p> 
						</td>
                     </tr>
            <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      </div>
      </div>
	  <?php
      include_once 'php/footer.php';
     ?>
	   <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Datos Insertados Correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"No se insertarón Datos\", \"error\");});</script>
				";
			}
		}
	 ?>
	   <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "eliminacion"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"!!! Datos eliminados correctamente !!!\", \"success\");});</script>";
			}else if($_GET["id"] == "erroreliminacion"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"!!! No se elimino ningun dato !!!\", \"error\");});</script>
				";
			}
		}
?> 
     
   </body>
</html>