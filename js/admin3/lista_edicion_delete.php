<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>
<?php
	require('php/config.php');
	
	$query="SELECT id_edicion, edicion, url_edicion, numero_edicion FROM edicion";
	
	$resultado=$con->query($query);
	
?>
<?php include('php/header.php'); ?>
     <h1>Eliminar Ediciones Anteriores</h1>

<div class="container">
  <div class="form-group">
  
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Ir al Panel de Administrador</a></li>
   
  </ul>
</div>
</div>

      <div class="form-group">
      <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="table-responsive">
               <table id="mytable" class="table table-bordred table-striped">
                  <thead>
                     <th>ID Edición</th>
                     <th>Nombre Edición</th>
                     <th>URL </th>
                     <th>N° Edición</th>
                    
                  </thead>
                  <tbody>
				    <?php while($row=$resultado->fetch_assoc()){ ?>
                     <tr>
                         <td><?php echo $row['id_edicion'];?></td>
                        <td><?php echo $row['edicion'];?></td>
                        <td><?php echo $row['url_edicion'];?></td>
                        <td><?php echo $row['numero_edicion'];?></td>
                        <td>
                        	<p data-placement="top" data-toggle="tooltip" title="Actualizar"><a class="btn btn-danger btn-xs" href="eliminar_edicion.php?id=<?php echo $row['id'];?>"><span class="glyphicon glyphicon-trash"></span></a></p>

						
						</td>
                       
                     </tr>
            <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      </div>
      </div>
	   <?php
      include_once 'php/footer.php';
     ?>
	   <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Datos Insertados Correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"No se insertarón Datos\", \"error\");});</script>
				";
			}
		}
	 ?>
	   <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "eliminacion"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"!!! Datos eliminados correctamente !!!\", \"success\");});</script>";
			}else if($_GET["id"] == "erroreliminacion"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"!!! No se elimino ningun dato !!!\", \"error\");});</script>
				";
			}
		}
?> 

   </body>
</html>