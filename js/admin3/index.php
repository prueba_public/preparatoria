<?php
   require_once("php/sesion.class.php");
   require_once("php/config.php");
   
   $sesion = new sesion();
   
   if( isset($_POST["iniciar"]) )
   {
   	
   	$email= $_POST["email"];
   	$contrasena = $_POST["contrasena"];
   	
   	if(validarUsuario($email,$contrasena) == true)
   	{			
   	 $sesion->set("email",$email);
     $consulta = "select nombre,apellidos,privilegio,url from usuarios where email = '$email';";
     $result = $con->query($consulta);
     
     if ($result->num_rows > 0) {
      
        while($row = $result->fetch_assoc()) {
          
     	$nombre=$row['nombre'];
        $privilegio=$row['privilegio'];
     	$apellidos=$row['apellidos'];
     	$imagen_perfil=$row['url'];
        }
     } 
   		$sesion->set_datos("nombre",$nombre);
   		$sesion->set_privilegio("privilegio",$privilegio);
   		$sesion->set_apellidos("apellidos",$apellidos);
   		$sesion->set_imagenperfil("url",$imagen_perfil);
   		
   	
   		header("location: admin.php?id=correcto");
   	}
   	else 
   	{
   		/*echo "Verifica tu nombre de email y contrasena";*/
   		header("Location: index.php?id=incorrecto");
   		
   	}
   }
   function validarUsuario($email, $contrasena)
   
   {
   include 'php/config.php';
   	//$conexion = new mysqli("localhost","root","190789","aem");
   	$consulta = "select contrasena from usuarios where email = '$email';";
   	
   	$result = $con->query($consulta);
   	
   	if($result->num_rows > 0)
   	{
   		$fila = $result->fetch_assoc();
   		if( strcmp($contrasena,$fila["contrasena"]) == 0 )
   			return true;						
   		else					
   			return false;
   	}
   	else
   			return false;		
   			
   }
   
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Sesión</title>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <link rel="stylesheet" href="css/AdminLTE.min.css">
      <link rel="stylesheet" href="css/sweetalert.css">
      <link rel="stylesheet" href="css/stylos.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> 
   </head>
   <body>
      <div class="login-box">
         <div class="login-logo">
            <a><b>Admin </b> AEM</a>
         </div>
         <div class="login-box-body">
            <p class="login-box-msg"><b>Inicio de Sesión </b></p>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
               <div class="form-group has-feedback">
                  <input type="email" class="form-control" name="email" placeholder="Email" required title="Email">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
               <div class="form-group has-feedback">
                  <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Password" required title="Contraseña">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
               </div>
               <div class="col-xs-8">
                  <div class="checkbox ">
                     <label>
                     <input type="checkbox" id="mostrar_contrasena" class="checkbox"> <a> Mostrar Contarseña </a>
                     </label>
                  </div>
               </div>
               <!-- /.col -->
               <div class="row">
                  <div class="col-xs-8">
                  </div>
                  <div class="social-auth-links text-center">
                     <input type="submit" class="btn btn-primary btn-lg btn-flat" name ="iniciar" value="Iniciar Sesión">
                  </div>
               </div>
            </form>
            <a href="#">Recuperar Password</a><br>
         </div>
      </div>
      <?php 
         if(isset($_GET["id"]) && !empty($_GET["id"])){
         	if($_GET["id"] == "incorrecto"){
         		echo "
         		<script>jQuery(function(){swal(\"¡Error!\", \"Error Datos Incorrectos\", \"error\");});</script>
         		";
         	}
         	
         	
         	
         	
         }
         ?>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/icheck.min.js"></script>
      <script src="js/valida.js"></script>
      <script src="js/sweetalert.min.js"></script>
   </body>
</html>
