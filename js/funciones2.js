
var ww = $(window).width();
var wh = $(window).height();
var flag = 0;


$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});




$(document).ready(function() {

	$('#loader').hide();
	$('#caja-flotante').animateCss('fadeIn');
	
	$('.bxslider').bxSlider({
		pager: true,
		auto:true,
		nextSelector: '#nextArrow',
		prevSelector: '#prevArrow',
		nextText: '<img src="img/bannerDerechaflecha.png">',
		prevText: '<img src="img/bannerIzquierdaflecha.png">'
		
	});

	$('.sliderArticulos').bxSlider({	  
		slideWidth: 330,
		auto:true,
		pager: false,  
		minSlides: 1,
	    maxSlides: 3,
	    moveSlides:1,
	    nextSelector: '#next',
		prevSelector: '#prev',
		nextText: '<img src="img/fder.png">',
		prevText: '<img src="img/fizq.png">'
	   
	  });

	$('#sliderNoticias').bxSlider({	  
		slideWidth: 330,
		auto:false,
		pager: false,  
		minSlides: 1,
	    maxSlides: 3,
	    moveSlides:1,
	    nextSelector: '#nextNoticias',
		prevSelector: '#prevNoticias',
		nextText: '<img src="img/fder.png">',
		prevText: '<img src="img/fizq.png">'
	   
	  });


	$('#pantallaInicial').width(ww).height(wh);

	$('#imgBienvenida').width(ww).height(wh);
	
	

	


	$('#menuAlterno').click(function() {
	  var clicks = $(this).data('clicks');
	  if (clicks) {
	     $('#menuSme').show();
	  } else {
	     $('#menuSme').fadeOut();
	  }
	  $(this).data("clicks", !clicks);
	});

	

	//HEADER

		var alturah = $('#header').height();
		var caja = $("#caja-flotante").height();
		var obj = $(document); 	

        $(document).scroll(function(){
         	var obj_act_top = $(this).scrollTop();  //obtener scroll top instantaneo
         	var entero = parseInt(obj_act_top);
            

            if(entero > caja){
           		$("#caja-flotante").hide();  

      		}    

      		if($('#caja-flotante').css('display') === 'none'){
	      		if ($(document).scrollTop() >= alturah){
	               $('.navbar').css({
	                  'position':'fixed',
	                  'width':'100%',
	                  'z-index':'10',
	                  'top':'0'
	               });

	            } 
	            if ($(document).scrollTop() <= alturah){
	               $('.navbar').css({
	                  'position':'',
	                  'width':'100%',
	                  'z-index':'10',
	                  'top':""
	               })
	            }
      		}  



        });

})	

/*div al elemento a ocultar*/
function muestra_oculta(id){
if (document.getElementById){ //se obtiene el id
var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
}
}

$(window).on('resize', function(){
	ww = $(window).width();
	wh = $(window).height();
	$('#pantallaInicial').height(wh).width(ww);
	$('#imgBienvenida').width(ww).height(wh);
})

$(function() {

	$('.pic').each( function() { $(this).hoverdir({
		hoverDelay : 75
	}); } );

});



function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}

  

 $(function(d,s,id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }
    (document, 'script', 'facebook-jssdk'));

  /**
         * Array con las imagenes que se iran mostrando en la web
         */
         var imagenes=new Array(
         
             'img/bienvenida/Intro_1.jpg',
             'img/bienvenida/Intro_2.jpg',
			 'img/bienvenida/Intro_3.jpg',
			 'img/bienvenida/Intro_4.jpg'
			
			 
             
         );
         
     
	  if(ww < 760){
    	$('.menu').fadeOut();
    	$('.btnCerrar').fadeIn();
    	$('#next').hide();
    	$('#prev').hide();
    	$('#nextNoticias').hide();
    	$('#prevNoticias').hide();
    	 function AbrirVentana() {
    	    document.getElementById("menu_responsive").style.width = "100%";
    	}

    	function CerrarVentana() {
    	    document.getElementById("menu_responsive").style.width = "0%";
    	}
    }

santyLibBgAleat=function(){this.v="1.0";this.imagenes=function(){var x=arguments,img=this.a(x);this.s(img)};this.a=function(r){var a=Math.random()*r.length;a=Math.floor(a);return(r[a])};this.c=function(b){head=document.getElementsByTagName("head")[0];if(!head)return;var s=document.createElement("style");s.type='text/css';s.innerHTML=b;head.appendChild(s)};this.s=function(b){var o=undefined;b.css=(b.css!==o)?b.css:"";b.url=(b.url!==o)?b.url:"";this.c("#pantallaInicial {background:url('"+b.url+"') "+b.css+";background-size: cover;}")}};$santyBA=new santyLibBgAleat();
 
	  $santyBA.imagenes(
    {url:'img/bienvenida/Intro_1.jpg',css:'no-repeat center center '},
    {url:'img/bienvenida/Intro_2.jpg',css:'no-repeat center center '},
	{url:'img/bienvenida/Intro_3.jpg',css:'no-repeat center center '},
	{url:'img/bienvenida/Intro_4.jpg',css:'no-repeat center center '}
    );

  




