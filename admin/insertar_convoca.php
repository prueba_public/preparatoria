<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>
<?php include('php/header.php'); 
?>
<h1>Crear Convocatoria</h1>
<div class="container">
  <div class="form-group">
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Ir al Panel de Adminsitrador</a></li>
  </ul>
</div></div>
      <div class="form-group">
      <div class="container" id="log">
         <form action="php/inserta_convocatorias.php" method="post" enctype="multipart/form-data">
           <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Titulo de la Convocatoria</label>
                  <input type="text" class="form-control" placeholder="Titulo" id="titulo_convocatoria" name="titulo_convocatoria">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			<div class="row control-group">
               <div class="form-group col-xs-6 ">
                   <label>Nombre del Autor</label>
                  <input type="text" class="form-control" placeholder="Autor" id="autor" name="autor_convocatoria">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>Fecha de Publicación</label>
                  <input type="date" class="form-control" placeholder="Fecha" id="fecha" name="fecha_convocatoria" required title="fecha">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			<div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                 <div class="form-group">
                <label for="comment">Introducción de la Convocatoria</label>
                <textarea class="form-control" rows="5" id="comment" name="intro_convocatoria"></textarea>
                 </div>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Descripción Completa de la Convocatoria</label>
                 <textarea name="articulo_convocatoria" id="articulo_convocatoria" cols="30" rows="10"></textarea>
                 <script type="text/javascript">
		         CKEDITOR.replace('articulo_convocatoria');
		
		         </script>
               </div>
			   
            </div>
			<div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
			   	<div class="form-group">
				 <label>Seleccionar Imagen</label>
                 </div>
				 <input id="image" type="file" name="imagen"  REQUIRED>
               </div>
            </div>
           
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Crear Convocatoria">
            <input type="reset" class="btn btn-danger " name="cancel"  value="Cancelar">
         </form>
      </div>
      </div>
	  <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Edicion creada correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"No se creo la Edicion\", \"error\");});</script>
				";
			}
		}
	 ?>
    <?php
      include_once 'php/footer.php';
     ?>
   </body>
</html>