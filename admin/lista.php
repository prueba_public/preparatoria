<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>
<?php
	require('php/config.php');
	$privilegio = $_GET['privilegio'];
	$query="SELECT id, titulo, subtitulo, autor FROM articulos where tipo_usuario='$privilegio'";
	
	$resultado=$con->query($query);
	
?>
<?php include('php/header.php'); ?>
     <h1>Lista de Articulos de la Revista AEM</h1>

<div class="container">
  <div class="form-group">
  
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Ir al Panel de Administrador</a></li>
   
  </ul>
</div>
</div>

      <div class="form-group">
      <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="table-responsive">
               <table id="mytable" class="table table-bordred table-striped">
                  <thead>
                     <th>ID Articulo</th>
                     <th>Titulo</th>
                     <th>Subtitulo</th>
                     <th>Autor</th>
                    
                  </thead>
                  <tbody>
				    <?php while($row=$resultado->fetch_assoc()){ ?>
                     <tr>
                         <td><?php echo $row['id'];?></td>
                        <td><?php echo $row['titulo'];?></td>
                        <td><?php echo $row['subtitulo'];?></td>
                        <td><?php echo $row['autor'];?></td>
                        <td>
                        	<p data-placement="top" data-toggle="tooltip" title="Actualizar"><a class="btn btn-primary btn-xs" href="actualiza.php?id=<?php echo $row['id'];?>"><span class="glyphicon glyphicon-refresh"></span></a></p>

						
						</td>
                       
                     </tr>
            <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      </div>
      </div>
	   <?php
      include_once 'php/footer.php';
     ?>
	   <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "correcto"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"Datos Insertados Correctamente\", \"success\");});</script>";
			}else if($_GET["id"] == "incorrecto"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"No se insertarón Datos\", \"error\");});</script>
				";
			}
		}
	 ?>
	   <?php 
		if(isset($_GET["id"]) && !empty($_GET["id"])){
			if($_GET["id"] == "eliminacion"){
				echo "<script>jQuery(function(){swal(\"¡¡ OK !!\", \"!!! Datos eliminados correctamente !!!\", \"success\");});</script>";
			}else if($_GET["id"] == "erroreliminacion"){
				echo "
				<script>jQuery(function(){swal(\"¡Error!\", \"!!! No se elimino ningun dato !!!\", \"error\");});</script>
				";
			}
		}
?> 

   </body>
</html>