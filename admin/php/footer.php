 <div class="container-fluid fondoAzulfuerte">
         <div class="row">
            <div class="col-md-2">
               <div id="logoFooter">
                  <img src="img/logo.gif" id="logoFooter">
               </div>
            </div>
            <div class="col-md-3">
               <div>
                  <img  src="img/header/stcaem.png" id="logosFooter">
               </div>
            </div>
            <div class="col-md-4">
               <div id="descripcion">
                  Revista hacia el espacio de divulgación de la ciencia y tecnologia espacial de la <span class="bold">Agencia espacial Méxicana.</span><br><br>
                  <div style="text-transform: capitalize;">Acerca de nuestra politica de privacidad</div>
               </div>
               
            </div>
            <div class="col-md-3">
               <div id="redesFooter">
                  <i class="fa fa-youtube-square"></i>
                  <i class="fa fa-twitter-square"></i>
                  <i class="fa fa-facebook-square"></i>
                  <i class="fa fa-video-camera"></i>
               </div>
            </div>
         </div>
      </div>