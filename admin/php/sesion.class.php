<?php
class sesion {
  function __construct() {
     session_start ();
  }
  public function set($email, $valor) {
     $_SESSION [$email] = $valor;
	 
  }
  public function get($email) {
     if (isset ( $_SESSION [$email] )) {
        return $_SESSION [$email];
     } else {
         return false;
     } 
  }
  
  /*extrar nombre*/
  public function set_datos($nombre,$valor) {
     $_SESSION [$nombre] = $valor;
	 
  }
  public function get_datos($nombre) {
      if (isset ( $_SESSION [$nombre] )) {
        return $_SESSION [$nombre];
     } else {
         return false;
     } 
  }
   /*extrar privilegio*/
  public function set_privilegio($privilegio,$valor) {
     $_SESSION [$privilegio] = $valor;
	 
  }
  public function get_privilegio($privilegio) {
      if (isset ( $_SESSION [$privilegio] )) {
        return $_SESSION [$privilegio];
     } else {
         return false;
     } 
  }
    /*extrar apellidos*/
  public function set_apellidos($apellidos,$valor) {
     $_SESSION [$apellidos] = $valor;
	 
  }
  public function get_apellidos($apellidos) {
      if (isset ( $_SESSION [$apellidos] )) {
        return $_SESSION [$apellidos];
     } else {
         return false;
     } 
  }
   /*nueva funcion para rescatar la imagen usuario*/
   public function set_imagenperfil($imagen_perfil,$valor) {
     $_SESSION [$imagen_perfil] = $valor;
	 
  }
  public function get_imagenperfil($imagen_perfil) {
      if (isset ( $_SESSION [$imagen_perfil] )) {
        return $_SESSION [$imagen_perfil];
     } else {
         return false;
     } 
  }
   /*termina la sesion*/
  public function elimina_variable($email) {
      unset ( $_SESSION [$email] );
  }
  public function termina_sesion() {
      $_SESSION = array();
      session_destroy ();
  }
}
?>