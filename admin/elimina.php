<?php
	require_once("php/sesion.class.php");
	
	
	$sesion = new sesion();
	$email = $sesion->get("email");
	
	if( $email == false )
	{	
		header("Location: index.php");		
	}
	?>

<?php include('php/header.php'); ?>

          <?php
         require_once ('php/config.php');
         $id= $_GET['id'];
         $sql = "SELECT * FROM articulos where id=$id";
         $result = $con->query($sql);

         if ($result) {
              // obtine los valores por medio del id de las columnas de la tabla 
              while($row = $result->fetch_assoc()) {
			  
?>
 <h1>Eliminar Articulos de la Revista AEM</h1>

<div class="container">
  <div class="form-group">
  
  <ul class="controls">
    <li class="control-prev"><a href="admin.php">&lt; Seguir Eliminando</a></li>
   
  </ul>
</div></div>

 <div class="form-group">
 <div class="container">
         <form action="php/eliminar.php" method="post">
		<input type="hidden" name="id"  value="<?php echo $id;?>">
            <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Titulo del Articulo</label>
                  <input type="text" class="form-control" placeholder="Titulo" id="titulo" name="titulo"  required title="Titulo" value="<?php echo $row['titulo']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label>SubTitulo</label>
                  <input type="text" class="form-control" placeholder="SubTitulo" id="Subtitulo" name="subtitulo"  value="<?php echo $row['subtitulo']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			<div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Articulo</label>
                  <textarea name="articulo" id="articulo" cols="30" rows="10"><?php echo $row['articulo']?></textarea>
                 <script type="text/javascript">
		         CKEDITOR.replace('articulo');
		
		         </script>
               </div>
			   
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Nombre del Autor</label>
                  <input type="text" class="form-control" placeholder="Autor" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>URL Imagen</label>
                  <input type="text" class="form-control" placeholder="URL" id="imagen" name="imagen" required title="imagen" value="<?php echo $row['imagen']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>URL Video</label>
                  <input type="url" class="form-control" placeholder="URL" id="video" name="video"  value="<?php echo $row['video']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Galeria</label>
                  <input type="number" class="form-control" placeholder="Galeria" id="galeria" name="galeria" required title="Galeria" value="<?php echo $row['galeria']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                  <label>Tags</label>
                  <input type="text" class="form-control" placeholder="tags" id="tags" name="tags"  required title="Tags" value=" <?php echo $row['tags']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label for="categoria">Categoria</label>
                  <select class="form-control" id="categoria" name="categoria" required title="opcion"  value="<?php echo $row['categoria']?>">
                     <option><?php echo $row['categoria']?></option>
                    
                  </select>
               </div>
               <div class="form-group col-xs-6 ">
                  <label for="seccion">Sección</label>
                  <select class="form-control" id="seccion" name="seccion"  required title="opcion"  value="<?php echo $row['seccion']?>">
                     <option><?php echo $row['seccion']?></option>
                    
                  </select>
               </div>
            </div>
           			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Numero de Publicación</label>
                  <input type="number" class="form-control" placeholder="N° Publicación" id="publicacion" name="publicacion" required title="Publicación" value="<?php echo $row['publicacion']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>Columna/art.</label>
                  <input type="text" class="form-control" placeholder="Columna/Articulo" id="columna_articulo" name="columna_articulo"  title="columna_articulo" value="<?php echo $row['columna_art']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			 <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Pie de Imagen</label>
                  <input type="text" class="form-control" placeholder="Pie de Imagen" id="pie_de_imagen"  name="pie_de_imagen"  value="<?php echo $row['pie_de_imagen']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>Pie de Video</label>
                  <input type="text" class="form-control" placeholder="Pie de Video" id="pie_de_video" name="pie_de_video"   value="<?php echo $row['pie_de_video']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
            <div class="row control-group">
               <div class="form-group col-xs-6 ">
                  <label>Fecha</label>
                  <input type="date" class="form-control" placeholder="Fecha" id="fecha"  name="fecha" required title="Fecha" value="<?php echo $row['fecha']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
               <div class="form-group col-xs-6 ">
                   <label>Referencias</label>
                  <input type="text" class="form-control" placeholder="Referencia" id="referencia" name="referencia"   value="<?php echo $row['referencia']?>">
                  <p class="help-block text-danger">
                  </p>
               </div>
            </div>
			
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Eliminar Articulo">
			<a href="admin.php" class="btn btn-danger ">Cancelar</a>
         </form>
      </div>
      </div>
	  <?php
      include_once 'php/footer.php';
     ?>
<?php
             }

			 } 
         

         $con->close();
?>
   </body>
</html>