<?php
   require_once("php/sesion.class.php");
   
   
   $sesion = new sesion();
   $email = $sesion->get("email");
   
   if( $email == false )
   {	
   	header("Location: index.php");		
   }
   ?>
<?php include('php/header.php'); ?>
<?php
   require_once ('php/config.php');
   $id= $_GET['id'];
   $status= $_GET['sta'];
   $sql = "SELECT * FROM articulos where id=$id";
   $result = $con->query($sql);
   
   if ($result) {
        // obtine los valores por medio del id de las columnas de la tabla 
        while($row = $result->fetch_assoc()) {
   $privilegio=$row['tipo_usuario'];
   
   ?>
<h1>Seccion de Articulos de la Revista AEM</h1>
<?php
   if($status=="2"){
   ?>
<div class="container">
   <div class="form-group">
      <ul class="controls">
         <li style="color:red;text-align:center;font-size: 20px;"> No se actualizo la imagen tiene que ser png o jpg y maximo de 800kb</li>
         <br>
      </ul>
   </div>
</div>
<?php
   }else if($status=="3"){
   ?>
<div class="container">
   <div class="form-group">
      <ul class="controls">
         <li  style="color:red;text-align:center;font-size: 20px;">No se actualizo, intentalo de nuevo</li>
         <br>
      </ul>
   </div>
</div>
<?php
   } if( $privilegio=="Editor"){
   ?>
<div class="container">
   <div class="form-group">
      <ul class="controls">
         <li class="control-prev"><a href="admin.php">&lt; Seguir Actualizando</a></li>
         <br>
      </ul>
   </div>
</div>
<div class="form-group">
   <div class="container">
      <form action="php/actualizar.php" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id"  value="<?php echo $id;?>">
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Titulo del Articulo</label>
               <input type="text" class="form-control" placeholder="Titulo" id="titulo" name="titulo"  required title="Titulo" value="<?php echo $row['titulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Subtitulo</label>
               <input type="text" class="form-control" placeholder="Subtitulo" id="Subtitulo" name="subtitulo"  value="<?php echo $row['subtitulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Descripción</label>
               <textarea class="form-control" id="descripcion1" name="descripcion" cols="30" rows="8"><?php echo $row['descripcion']?></textarea>
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Articulo</label>
               <textarea name="articulo" id="articulo" cols="30" rows="10"><?php echo $row['articulo']?></textarea>
               <script type="text/javascript">
                  CKEDITOR.replace('articulo', {
                         extraPlugins: 'imageuploader',
                         toolbar : 'full'
                       });
                  
                   
               </script>
            </div>
         </div>
         <!--<div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" placeholder="Autor" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            </div>-->
         <div class="row control-group">
            <div class="form-group col-xs-6">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tipo de Usuario</label>
               <input type="text" class="form-control" placeholder="tipo_usuario" id="tipo_usuario" name="tipo_usuario" value="<?php echo $privilegio; ?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>URL Video</label>
               <input type="url" class="form-control" placeholder="URL" id="video" name="video"  value="<?php echo $row['video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Galeria</label>
               <input type="number" class="form-control" placeholder="Galeria" id="galeria" name="galeria" required title="Galeria" value="<?php echo $row['galeria']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tags</label>
               <input type="text" class="form-control" placeholder="tags" id="tags" name="tags"  required title="Tags" value=" <?php echo $row['tags']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label for="categoria">Categoria</label>
               <select class="form-control" id="categoria" name="categoria" required title="opcion" >
                  <option <?php if($row['categoria'] == "Noticias"){echo 'selected';}?> >Noticias</option>
                  <option <?php if($row['categoria'] == "Secciones"){echo 'selected';}?> >Secciones</option>
                  <option <?php if($row['categoria'] == "Articulos"){echo 'selected';}?> >Articulos</option>
               </select>
            </div>
            <div class="form-group col-xs-6 ">
               <label for="seccion">Sección</label>
               <select class="form-control" id="seccion" name="seccion" title="opcion"  value="<?php echo $row['seccion']?>">
                  <option><?php echo $row['seccion']?></option>
                  <option value="¿Quien es Quién?">¿Quien es Quién?</option>
                  <option value="Mirando las Estrellas">Mirando las Estrellas</option>
                  <option value="Recordando a...">Recordando a...</option>
                  <option value="Recomendación del Mes">Recomendación del Mes</option>
                  <option value="Jóvenes Talento">Jóvenes Talento</option>
                  <option value="¿Sabias que..?">¿Sabias que..?</option>
                  <option value="Espacio GEEk">Espacio GEEk</option>
                  <option value="Espacio de lo niños">Espacio de lo niños</option>
               </select>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Numero de Publicación</label>
               <input type="number" class="form-control" placeholder="N° Publicación" id="publicacion" name="publicacion" required title="Publicación" value="<?php echo $row['publicacion']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Columna/art.</label>
               <input type="text" class="form-control" placeholder="Columna/Articulo" id="columna_articulo" name="columna_articulo"  title="columna_articulo" value="<?php echo $row['columna_art']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Pie de Imagen</label>
               <input type="text" class="form-control" placeholder="Pie de Imagen" id="pie_de_imagen"  name="pie_de_imagen"  value="<?php echo $row['pie_de_imagen']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Pie de Video</label>
               <input type="text" class="form-control" placeholder="Pie de Video" id="pie_de_video" name="pie_de_video"   value="<?php echo $row['pie_de_video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Fecha</label>
               <input type="date" class="form-control" placeholder="Fecha" id="fecha"  name="fecha" required title="Fecha" value="<?php echo $row['fecha']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Referencias</label>
               <input type="text" class="form-control" placeholder="Referencia" id="referencia" name="referencia"   value="<?php echo $row['referencia']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <div class="form-group">
                  <label>Esta es tu imagen actual, si deseas cambiarla elige otra imagen en seleccionar archivo</label>
               </div>
               <div class="form-group">
                  <div style="width:250px;"><img src="../<?php echo $row['imagen']?>" style="width:100%;" /></div>
                  <input id="image" type="hidden" value="<?php echo $row['imagen']?>" name="imagenAnterior">
               </div>
               <input id="image" type="file" name="imagen" >
            </div>
         </div>
         <div class="row control-group" style="text-align:right;">
            <input type="submit" class="btn btn-success " name="aprobar" id="aprobar" value="Aprobar Articulo">
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Editar Articulo">
            <a href="admin.php" class="btn btn-danger ">Cancelar</a>
         </div>
         <!-- <button type="reset" class="btn btn-danger" >Cancelar</button>-->
      </form>
   </div>
</div>
<?php
   }else if( $privilegio=="Colaborador"){
     
     ?>
<div class="container">
   <div class="form-group">
      <ul class="controls">
         <li class="control-prev"><a href="admin.php">&lt; Seguir Actualizando</a></li>
         <br>
      </ul>
   </div>
</div>
<div class="form-group">
   <div class="container">
      <form action="php/actualizar.php" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id"  value="<?php echo $id;?>">
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Titulo del Articulo</label>
               <input type="text" class="form-control" placeholder="Titulo" id="titulo" name="titulo"  required title="Titulo" value="<?php echo $row['titulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Subtitulo</label>
               <input type="text" class="form-control" placeholder="Subtitulo" id="Subtitulo" name="subtitulo"  value="<?php echo $row['subtitulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Descripción</label>
               <textarea class="form-control" id="descripcion1" name="descripcion" cols="30" rows="8"><?php echo $row['descripcion']?></textarea>
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Articulo</label>
               <textarea name="articulo" id="articulo" cols="30" rows="10"><?php echo $row['articulo']?></textarea>
               <script type="text/javascript">
                  CKEDITOR.replace('articulo', {
                         extraPlugins: 'imageuploader',
                         toolbar : 'full'
                       });
                  
                   
               </script>
            </div>
         </div>
         <!--<div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" placeholder="Autor" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            </div>-->
         <div class="row control-group">
            <div class="form-group col-xs-6">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tipo de Usuario</label>
               <input type="text" class="form-control" placeholder="tipo_usuario" id="tipo_usuario" name="tipo_usuario" value="<?php echo $privilegio; ?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>URL Video</label>
               <input type="url" class="form-control" placeholder="URL" id="video" name="video"  value="<?php echo $row['video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Galeria</label>
               <input type="number" class="form-control" placeholder="Galeria" id="galeria" name="galeria" required title="Galeria" value="<?php echo $row['galeria']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tags</label>
               <input type="text" class="form-control" placeholder="tags" id="tags" name="tags"  required title="Tags" value=" <?php echo $row['tags']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label for="categoria">Categoria</label>
               <select class="form-control" id="categoria" name="categoria" required title="opcion" >
                  <option <?php if($row['categoria'] == "Noticias"){echo 'selected';}?> >Noticias</option>
                  <option <?php if($row['categoria'] == "Secciones"){echo 'selected';}?> >Secciones</option>
                  <option <?php if($row['categoria'] == "Articulos"){echo 'selected';}?> >Articulos</option>
               </select>
            </div>
            <div class="form-group col-xs-6 ">
               <label for="seccion">Sección</label>
               <select class="form-control" id="seccion" name="seccion" title="opcion"  value="<?php echo $row['seccion']?>">
                  <option><?php echo $row['seccion']?></option>
                  <option value="¿Quien es Quién?">¿Quien es Quién?</option>
                  <option value="Mirando las Estrellas">Mirando las Estrellas</option>
                  <option value="Recordando a...">Recordando a...</option>
                  <option value="Recomendación del Mes">Recomendación del Mes</option>
                  <option value="Jóvenes Talento">Jóvenes Talento</option>
                  <option value="¿Sabias que..?">¿Sabias que..?</option>
                  <option value="Espacio GEEk">Espacio GEEk</option>
                  <option value="Espacio de lo niños">Espacio de lo niños</option>
               </select>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Numero de Publicación</label>
               <input type="number" class="form-control" placeholder="N° Publicación" id="publicacion" name="publicacion" required title="Publicación" value="<?php echo $row['publicacion']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Columna/art.</label>
               <input type="text" class="form-control" placeholder="Columna/Articulo" id="columna_articulo" name="columna_articulo"  title="columna_articulo" value="<?php echo $row['columna_art']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Pie de Imagen</label>
               <input type="text" class="form-control" placeholder="Pie de Imagen" id="pie_de_imagen"  name="pie_de_imagen"  value="<?php echo $row['pie_de_imagen']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Pie de Video</label>
               <input type="text" class="form-control" placeholder="Pie de Video" id="pie_de_video" name="pie_de_video"   value="<?php echo $row['pie_de_video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Fecha</label>
               <input type="date" class="form-control" placeholder="Fecha" id="fecha"  name="fecha" required title="Fecha" value="<?php echo $row['fecha']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Referencias</label>
               <input type="text" class="form-control" placeholder="Referencia" id="referencia" name="referencia"   value="<?php echo $row['referencia']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <div class="form-group">
                  <label>Esta es tu imagen actual, si deseas cambiarla elige otra imagen en seleccionar archivo</label>
               </div>
               <div class="form-group">
                  <div style="width:250px;"><img src="../<?php echo $row['imagen']?>" style="width:100%;" /></div>
                  <input id="image" type="hidden" value="<?php echo $row['imagen']?>" name="imagenAnterior">
               </div>
               <input id="image" type="file" name="imagen" >
            </div>
         </div>
         <div class="row control-group" style="text-align:right;">
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Editar Articulo">
            <a href="admin.php" class="btn btn-danger ">Cancelar</a>
         </div>
         <!-- <button type="reset" class="btn btn-danger" >Cancelar</button>-->
      </form>
   </div>
</div>
<?php
   }else if($privilegio==""){
   ?>
<div class="container">
   <div class="form-group">
      <ul class="controls">
         <li class="control-prev"><a href="admin.php">&lt; Seguir Actualizando</a></li>
         <br>
      </ul>
   </div>
</div>
<div class="form-group">
   <div class="container">
      <form action="php/actualizar.php" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id"  value="<?php echo $id;?>">
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Titulo del Articulo</label>
               <input type="text" class="form-control" placeholder="Titulo" id="titulo" name="titulo"  required title="Titulo" value="<?php echo $row['titulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Subtitulo</label>
               <input type="text" class="form-control" placeholder="Subtitulo" id="Subtitulo" name="subtitulo"  value="<?php echo $row['subtitulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Descripción</label>
               <textarea class="form-control" id="descripcion1" name="descripcion" cols="30" rows="8"><?php echo $row['descripcion']?></textarea>
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Articulo</label>
               <textarea name="articulo" id="articulo" cols="30" rows="10"><?php echo $row['articulo']?></textarea>
               <script type="text/javascript">
                  CKEDITOR.replace('articulo', {
                         extraPlugins: 'imageuploader',
                         toolbar : 'full'
                       });
                  
                   
               </script>
            </div>
         </div>
         <!--<div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" placeholder="Autor" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            </div>-->
         <div class="row control-group">
            <div class="form-group col-xs-6">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tipo de Usuario</label>
               <input type="text" class="form-control"  id="tipo_usuario" name="tipo_usuario" value="<?php echo $row['tipo_usuario']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>URL Video</label>
               <input type="url" class="form-control" placeholder="URL" id="video" name="video"  value="<?php echo $row['video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Galeria</label>
               <input type="number" class="form-control" placeholder="Galeria" id="galeria" name="galeria" required title="Galeria" value="<?php echo $row['galeria']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tags</label>
               <input type="text" class="form-control" placeholder="tags" id="tags" name="tags"  required title="Tags" value=" <?php echo $row['tags']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label for="categoria">Categoria</label>
               <select class="form-control" id="categoria" name="categoria" required title="opcion" >
                  <option <?php if($row['categoria'] == "Noticias"){echo 'selected';}?> >Noticias</option>
                  <option <?php if($row['categoria'] == "Secciones"){echo 'selected';}?> >Secciones</option>
                  <option <?php if($row['categoria'] == "Articulos"){echo 'selected';}?> >Articulos</option>
               </select>
            </div>
            <div class="form-group col-xs-6 ">
               <label for="seccion">Sección</label>
               <select class="form-control" id="seccion" name="seccion" title="opcion"  value="<?php echo $row['seccion']?>">
                  <option><?php echo $row['seccion']?></option>
                  <option value="¿Quien es Quién?">¿Quien es Quién?</option>
                  <option value="Mirando las Estrellas">Mirando las Estrellas</option>
                  <option value="Recordando a...">Recordando a...</option>
                  <option value="Recomendación del Mes">Recomendación del Mes</option>
                  <option value="Jóvenes Talento">Jóvenes Talento</option>
                  <option value="¿Sabias que..?">¿Sabias que..?</option>
                  <option value="Espacio GEEk">Espacio GEEk</option>
                  <option value="Espacio de lo niños">Espacio de lo niños</option>
               </select>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Numero de Publicación</label>
               <input type="number" class="form-control" placeholder="N° Publicación" id="publicacion" name="publicacion" required title="Publicación" value="<?php echo $row['publicacion']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Columna/art.</label>
               <input type="text" class="form-control" placeholder="Columna/Articulo" id="columna_articulo" name="columna_articulo"  title="columna_articulo" value="<?php echo $row['columna_art']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Pie de Imagen</label>
               <input type="text" class="form-control" placeholder="Pie de Imagen" id="pie_de_imagen"  name="pie_de_imagen"  value="<?php echo $row['pie_de_imagen']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Pie de Video</label>
               <input type="text" class="form-control" placeholder="Pie de Video" id="pie_de_video" name="pie_de_video"   value="<?php echo $row['pie_de_video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Fecha</label>
               <input type="date" class="form-control" placeholder="Fecha" id="fecha"  name="fecha" required title="Fecha" value="<?php echo $row['fecha']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Referencias</label>
               <input type="text" class="form-control" placeholder="Referencia" id="referencia" name="referencia"   value="<?php echo $row['referencia']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <div class="form-group">
                  <label>Esta es tu imagen actual, si deseas cambiarla elige otra imagen en seleccionar archivo</label>
               </div>
               <div class="form-group">
                  <div style="width:250px;"><img src="../<?php echo $row['imagen']?>" style="width:100%;" /></div>
                  <input id="image" type="hidden" value="<?php echo $row['imagen']?>" name="imagenAnterior">
               </div>
               <input id="image" type="file" name="imagen" >
            </div>
         </div>
         <div class="row control-group" style="text-align:right;">
            <input type="submit" class="btn btn-success " name="aprobar" id="aprobar" value="Aprobar Articulo">
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Editar Articulo">
            <a href="admin.php" class="btn btn-danger ">Cancelar</a>
         </div>
         <!-- <button type="reset" class="btn btn-danger" >Cancelar</button>-->
      </form>
   </div>
</div>
<?php 
   }if($privilegio=="Administrador"){
?>

<div class="container">
   <div class="form-group">
      <ul class="controls">
         <li class="control-prev"><a href="admin.php">&lt; Seguir Actualizando</a></li>
         <br>
      </ul>
   </div>
</div>
<div class="form-group">
   <div class="container">
      <form action="php/actualizar.php" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id"  value="<?php echo $id;?>">
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Titulo del Articulo</label>
               <input type="text" class="form-control" placeholder="Titulo" id="titulo" name="titulo"  required title="Titulo" value="<?php echo $row['titulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Subtitulo</label>
               <input type="text" class="form-control" placeholder="Subtitulo" id="Subtitulo" name="subtitulo"  value="<?php echo $row['subtitulo']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Descripción</label>
               <textarea class="form-control" id="descripcion1" name="descripcion" cols="30" rows="8"><?php echo $row['descripcion']?></textarea>
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Articulo</label>
               <textarea name="articulo" id="articulo" cols="30" rows="10"><?php echo $row['articulo']?></textarea>
               <script type="text/javascript">
                  CKEDITOR.replace('articulo', {
                         extraPlugins: 'imageuploader',
                         toolbar : 'full'
                       });
                  
                   
               </script>
            </div>
         </div>
         <!--<div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" placeholder="Autor" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            </div>-->
         <div class="row control-group">
            <div class="form-group col-xs-6">
               <label>Nombre del Autor</label>
               <input type="text" class="form-control" id="autor" name="autor" required title="Autor" value="<?php echo $row['autor']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tipo de Usuario</label>
               <input type="text" class="form-control"  id="tipo_usuario" name="tipo_usuario" value="<?php echo $row['tipo_usuario']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <label>URL Video</label>
               <input type="url" class="form-control" placeholder="URL" id="video" name="video"  value="<?php echo $row['video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Galeria</label>
               <input type="number" class="form-control" placeholder="Galeria" id="galeria" name="galeria" required title="Galeria" value="<?php echo $row['galeria']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Tags</label>
               <input type="text" class="form-control" placeholder="tags" id="tags" name="tags"  required title="Tags" value=" <?php echo $row['tags']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label for="categoria">Categoria</label>
               <select class="form-control" id="categoria" name="categoria" required title="opcion" >
                  <option <?php if($row['categoria'] == "Noticias"){echo 'selected';}?> >Noticias</option>
                  <option <?php if($row['categoria'] == "Secciones"){echo 'selected';}?> >Secciones</option>
                  <option <?php if($row['categoria'] == "Articulos"){echo 'selected';}?> >Articulos</option>
               </select>
            </div>
            <div class="form-group col-xs-6 ">
               <label for="seccion">Sección</label>
               <select class="form-control" id="seccion" name="seccion" title="opcion"  value="<?php echo $row['seccion']?>">
                  <option><?php echo $row['seccion']?></option>
                  <option value="¿Quien es Quién?">¿Quien es Quién?</option>
                  <option value="Mirando las Estrellas">Mirando las Estrellas</option>
                  <option value="Recordando a...">Recordando a...</option>
                  <option value="Recomendación del Mes">Recomendación del Mes</option>
                  <option value="Jóvenes Talento">Jóvenes Talento</option>
                  <option value="¿Sabias que..?">¿Sabias que..?</option>
                  <option value="Espacio GEEk">Espacio GEEk</option>
                  <option value="Espacio de lo niños">Espacio de lo niños</option>
               </select>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Numero de Publicación</label>
               <input type="number" class="form-control" placeholder="N° Publicación" id="publicacion" name="publicacion" required title="Publicación" value="<?php echo $row['publicacion']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Columna/art.</label>
               <input type="text" class="form-control" placeholder="Columna/Articulo" id="columna_articulo" name="columna_articulo"  title="columna_articulo" value="<?php echo $row['columna_art']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Pie de Imagen</label>
               <input type="text" class="form-control" placeholder="Pie de Imagen" id="pie_de_imagen"  name="pie_de_imagen"  value="<?php echo $row['pie_de_imagen']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Pie de Video</label>
               <input type="text" class="form-control" placeholder="Pie de Video" id="pie_de_video" name="pie_de_video"   value="<?php echo $row['pie_de_video']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-6 ">
               <label>Fecha</label>
               <input type="date" class="form-control" placeholder="Fecha" id="fecha"  name="fecha" required title="Fecha" value="<?php echo $row['fecha']?>">
               <p class="help-block text-danger">
               </p>
            </div>
            <div class="form-group col-xs-6 ">
               <label>Referencias</label>
               <input type="text" class="form-control" placeholder="Referencia" id="referencia" name="referencia"   value="<?php echo $row['referencia']?>">
               <p class="help-block text-danger">
               </p>
            </div>
         </div>
         <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
               <div class="form-group">
                  <label>Esta es tu imagen actual, si deseas cambiarla elige otra imagen en seleccionar archivo</label>
               </div>
               <div class="form-group">
                  <div style="width:250px;"><img src="../<?php echo $row['imagen']?>" style="width:100%;" /></div>
                  <input id="image" type="hidden" value="<?php echo $row['imagen']?>" name="imagenAnterior">
               </div>
               <input id="image" type="file" name="imagen" >
            </div>
         </div>
         <div class="row control-group" style="text-align:right;">
            <input type="submit" class="btn btn-success " name="aprobar" id="aprobar" value="Aprobar Articulo">
            <input type="submit" class="btn btn-primary " name="enviar" id="enviar" value="Editar Articulo">
            <a href="admin.php" class="btn btn-danger ">Cancelar</a>
         </div>
         <!-- <button type="reset" class="btn btn-danger" >Cancelar</button>-->
      </form>
   </div>
</div>





<?php
}
   }
   
   } 
      
   include_once 'php/footer.php';
      $con->close();
   ?>
<script>
   $(document).ready(function(){
   
   
     $('#categoria').change(function(){
      var categoria = $(this).val();
         switch(categoria){
            case "Articulos":
               $('#secciones').hide();
               break;
            case "Secciones":
               $('#secciones').show();
               break;
            case "Noticias":
               $('#secciones').hide();
               break;
            }
     })
   })
</script>
</body>
</html>